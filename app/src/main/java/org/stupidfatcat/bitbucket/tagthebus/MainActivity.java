package org.stupidfatcat.bitbucket.tagthebus;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.ShareActionProvider;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import org.json.JSONArray;
import org.stupidfatcat.bitbucket.tagthebus.connect.LocalConnect;
import org.stupidfatcat.bitbucket.tagthebus.fragments.AddPhotoFragment;
import org.stupidfatcat.bitbucket.tagthebus.fragments.GoogleMapFragment;
import org.stupidfatcat.bitbucket.tagthebus.fragments.PhotoListFragment;
import org.stupidfatcat.bitbucket.tagthebus.fragments.PhotoViewFragment;
import org.stupidfatcat.bitbucket.tagthebus.fragments.StationFragment;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * This MainActivity has a container and an ActionBar. The container is filled by fragments.
 * MainActivity controls all the fragments.
 */
public class MainActivity extends ActionBarActivity implements StationFragment.OnStationsReadyCallback, StationFragment.OnStationClickedCallback, AddPhotoFragment.OnPhotoSaveCallback, PhotoListFragment.OnPhotoClickedCallback {
    // static numbers of fragments
    private static final int STATION_FRAGMENT_NUM = 0;
    private static final int PHOTO_LIST_FRAGMENT_NUM = 1;
    private static final int ADD_PHOTO_FRAGMENT_NUM = 2;
    private static final int MAP_FRAGMENT_NUM = 3;
    private static final int PHOTO_VIEW_FRAGMENT_NUM = 4;

    // connection of sqlite
    private LocalConnect localConnect;

    // fragments
    private StationFragment stationFragment;
    private PhotoListFragment photoListFragment;
    private GoogleMapFragment mapFragment;
    private AddPhotoFragment addPhotoFragment;
    private PhotoViewFragment photoViewFragment;

    // the menu of activity
    private Menu menu;

    // to share photo
    private ShareActionProvider shareProvider = null;

    // The direction of the photo to share
    private String share_dir;

    // The data from RESTful api request
    private JSONArray arr_stations;

    // flag to control the appearance of fragments
    private boolean isStationListAdd = false;
    private boolean isMapAdd = false;
    private boolean isPhotoListAdd = false;
    private boolean isAddPhotoAdd = false;
    private boolean isPhotoViewAdd = false;

    // lastFrag: last shown fragment
    // thisFrag: current fragment
    private int lastFrag = -1;
    private int thisFrag = -1;

    /**
     * Choose fragment to show in container.
     *
     * @param int_fragment the static number of fragment
     */
    private void showFragment(int int_fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction ft = fragmentManager.beginTransaction();
        lastFrag = thisFrag;
        switch (int_fragment) {
            case STATION_FRAGMENT_NUM:
                // if STATION_FRAGMENT is not the current fragment, change to it.
                if (thisFrag != STATION_FRAGMENT_NUM) {
                    if (!isStationListAdd) {
                        ft.add(R.id.container, stationFragment);
                        isStationListAdd = true;
                    }
                    // hide the last fragment
                    hideLastFragment(ft);
                    ft.show(stationFragment);
                    thisFrag = int_fragment;
                }
                break;
            case MAP_FRAGMENT_NUM:
                if (thisFrag != MAP_FRAGMENT_NUM) {
                    if (!isMapAdd) {
                        ft.add(R.id.container, mapFragment);
                        isMapAdd = true;
                    }
                    hideLastFragment(ft);
                    ft.show(mapFragment);
                    thisFrag = int_fragment;
                }
                break;
            case PHOTO_LIST_FRAGMENT_NUM:
                if (thisFrag != PHOTO_LIST_FRAGMENT_NUM) {
                    if (!isPhotoListAdd) {
                        ft.add(R.id.container, photoListFragment);
                        isPhotoListAdd = true;
                    }
                    hideLastFragment(ft);
                    ft.show(photoListFragment);
                    thisFrag = int_fragment;
                }
                break;
            case ADD_PHOTO_FRAGMENT_NUM:
                if (thisFrag != ADD_PHOTO_FRAGMENT_NUM) {
                    if (!isAddPhotoAdd) {
                        ft.add(R.id.container, addPhotoFragment);
                        isAddPhotoAdd = true;
                    }
                    hideLastFragment(ft);
                    ft.show(addPhotoFragment);
                    thisFrag = int_fragment;
                }
                break;
            case PHOTO_VIEW_FRAGMENT_NUM:
                if (thisFrag != PHOTO_VIEW_FRAGMENT_NUM) {
                    if (!isPhotoViewAdd) {
                        ft.add(R.id.container, photoViewFragment);
                        isPhotoViewAdd = true;
                    }
                    hideLastFragment(ft);
                    ft.show(photoViewFragment);
                    thisFrag = int_fragment;
                }

        }
//        ft.commit();
        //fix bug of support.v4
        ft.commitAllowingStateLoss();
        changeActionBar(int_fragment);

    }

    /**
     * hide to last shown fragment in the container
     *
     * @param ft FragmentTransaction
     */
    private void hideLastFragment(FragmentTransaction ft) {
        switch (lastFrag) {
            case STATION_FRAGMENT_NUM:
                ft.hide(stationFragment);
                break;
            case MAP_FRAGMENT_NUM:
//                ft.remove(mapFragment);
//                isMapAdd = false;
                ft.hide(mapFragment);
                break;
            case PHOTO_LIST_FRAGMENT_NUM:
                ft.hide(photoListFragment);
                break;
            case ADD_PHOTO_FRAGMENT_NUM:
                ft.hide(addPhotoFragment);
                break;
            case PHOTO_VIEW_FRAGMENT_NUM:
                ft.hide(photoViewFragment);
                break;
        }
    }

    /**
     * control the order of fragment to show when click the button "back"
     */
    @Override
    public void onBackPressed() {
        switch (thisFrag) {
            case PHOTO_LIST_FRAGMENT_NUM:
                if (lastFrag == MAP_FRAGMENT_NUM) showFragment(MAP_FRAGMENT_NUM);
                else showFragment(STATION_FRAGMENT_NUM);
                break;
            case MAP_FRAGMENT_NUM:
                showFragment(STATION_FRAGMENT_NUM);
                break;
            case STATION_FRAGMENT_NUM:
                super.onBackPressed();
                break;
            case ADD_PHOTO_FRAGMENT_NUM:
                showFragment(PHOTO_LIST_FRAGMENT_NUM);
                break;
            case PHOTO_VIEW_FRAGMENT_NUM:
                showFragment(PHOTO_LIST_FRAGMENT_NUM);
                break;
        }
    }

    /**
     * set the path of photo to share
     *
     * @param dir photo path
     */
    private void setShareDir(String dir) {
        share_dir = dir;
    }

    /**
     * callback from PhotoListFragment when user clicked a item of ListView
     * when callback, PhotoListFragment passes 'name' and 'path' to the PhotoViewFragment
     * and PhotoViewFragment will use these params to show the photo
     *
     * @param name name of the selected photo
     * @param dir  the path od the selected photo
     */
    @Override
    public void onPhotoClickedCallback(String name, String dir) {
        photoViewFragment.setName(name);
        photoViewFragment.setDir(dir);
        setShareDir(dir);

        showFragment(PHOTO_VIEW_FRAGMENT_NUM);
        photoViewFragment.loadImage();
    }

    /**
     * callback from StationFragment  when the RESTful request from api succeeds.
     * when callback, MainActivity obtains the data from web server,
     * the data can be passed to GoogleMapFragment to add markers on the map.
     *
     * @param arr_stations array of stations
     */
    @Override
    public void onStationsReadyCallback(JSONArray arr_stations) {
        this.arr_stations = arr_stations;
    }

    /**
     * callback from StationFragment when user clicked a item of ListView
     * when callback, StationFragment passes 'id_station' and 'street_name' to the PhotoListFragment
     * PhotoListFragment will use these params to search all the path of photos at this station,
     * which is stored in SQLite, and to show on the PhotoListFragment
     *
     * @param id_station  id of station
     * @param street_name name of street
     */
    @Override
    public void onStationClickedCallback(int id_station, String street_name) {
        photoListFragment.setIdStation(id_station);
        photoListFragment.setStreetName(street_name);

        addPhotoFragment.setIdStation(id_station);
        showFragment(PHOTO_LIST_FRAGMENT_NUM);
        photoListFragment.loadPhotoList();

    }

    /**
     * callback from AddPhotoFragment when the photo taken from camera is saved.
     * And user will return to the PhotoListFragment
     */
    @Override
    public void onPhotoSaveCallback() {
        showFragment(PHOTO_LIST_FRAGMENT_NUM);
        photoListFragment.loadPhotoList();
    }

    /**
     * initialization of fragments and connection
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        localConnect = new LocalConnect(this);


        stationFragment = new StationFragment();
        photoListFragment = new PhotoListFragment();
        addPhotoFragment = new AddPhotoFragment();
        photoViewFragment = new PhotoViewFragment();

        photoListFragment.setLocalConnect(localConnect);
        addPhotoFragment.setLocalConnect(localConnect);

        setContentView(R.layout.container);

        showFragment(STATION_FRAGMENT_NUM);

    }

    /**
     * change the title, item of ActionBar when user is at different fragment
     *
     * @param int_fragment number of fragment
     */
    private void changeActionBar(int int_fragment) {
        if (menu != null) switch (int_fragment) {
            case STATION_FRAGMENT_NUM:
                hideItem(R.id.action_location);
                hideItem(R.id.action_add_photo);
                hideItem(R.id.action_share);
                showItem(R.id.action_update);
                showItem(R.id.action_map);
                setTitle(R.string.tag_the_bus);
                break;
            case MAP_FRAGMENT_NUM:
                hideItem(R.id.action_add_photo);
                hideItem(R.id.action_update);
                hideItem(R.id.action_map);
                hideItem(R.id.action_share);
                hideItem(R.id.action_location);
                // googlemap already has the button
                //showItem(R.id.action_location);
                setTitle(R.string.tag_the_bus);
                break;
            case PHOTO_LIST_FRAGMENT_NUM:
                hideItem(R.id.action_update);
                hideItem(R.id.action_map);
                hideItem(R.id.action_location);
                hideItem(R.id.action_share);
                showItem(R.id.action_add_photo);
                setTitle(photoListFragment.getStreetName());
                break;
            case ADD_PHOTO_FRAGMENT_NUM:
                hideItem(R.id.action_update);
                hideItem(R.id.action_map);
                hideItem(R.id.action_location);
                hideItem(R.id.action_add_photo);
                hideItem(R.id.action_share);
                setTitle("Add Photo");
                break;
            case PHOTO_VIEW_FRAGMENT_NUM:
                hideItem(R.id.action_update);
                hideItem(R.id.action_map);
                hideItem(R.id.action_location);
                hideItem(R.id.action_add_photo);
                showItem(R.id.action_share);
                setTitle(photoViewFragment.getName());
                break;


        }
    }

    /**
     * hide item of menu
     *
     * @param id
     */
    private void hideItem(int id) {
        MenuItem item = menu.findItem(id);
        item.setVisible(false);
    }

    /**
     * show item of menu
     *
     * @param id
     */
    private void showItem(int id) {
        MenuItem item = menu.findItem(id);
        item.setVisible(true);
    }

    /**
     * create an intent to share the photo
     */
    private void sharePhoto() {
        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.setType("image/jpeg");
        File photoToShare = new File(share_dir);

        shareIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(photoToShare));
        shareProvider.setShareIntent(shareIntent);

    }

    /**
     * prepare the file to save the photo
     * and open camera to capture picture.
     */
    private void openCamera() {
        File tempPhoto = null;
        try {
            // get date and prepare the name of photo file and thumbnail.
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy K:m a", java.util.Locale.getDefault());
            SimpleDateFormat sdf_file = new SimpleDateFormat("yyyy-MM-dd-hhmmss", java.util.Locale.getDefault());
            Date mDate = new Date();
            String date = sdf.format(mDate);
            String date_file = sdf_file.format(mDate);
            String imageFileName = "TagTheBus_" + date_file;
//            File storageDir = getExternalFilesDir(null);
            File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES + "/TagTheBus");
            File storageThumbDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES + "/TagTheBus/.thumbs");

            // if the folders doesn't exist, create them.
            if (!storageDir.exists()) {
                if (!storageDir.mkdir()) {
                    Toast toast = Toast.makeText(this, "can not create folder:" + storageDir.getAbsolutePath(), Toast.LENGTH_SHORT);
                    toast.show();
                }
            } else if (!storageThumbDir.exists()) {
                if (!storageThumbDir.mkdir()) {
                    Toast toast = Toast.makeText(this, "can not create folder:" + storageThumbDir.getAbsolutePath(), Toast.LENGTH_SHORT);
                    toast.show();
                }
            }

            //pass the file,and ready to add in AddPhotoFragment
            tempPhoto = File.createTempFile(imageFileName, ".jpg", storageDir);
            addPhotoFragment.setFullSizePhoto(tempPhoto);
            addPhotoFragment.setPhotoName(imageFileName);
            addPhotoFragment.setDate(date);
        } catch (IOException ex) {
            Log.e("Caught IOException: ", ex.getMessage());
        }

        // if the file is ready, open camera
        if (tempPhoto != null) {
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

            intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(tempPhoto));
            startActivityForResult(intent, 0);
        }
    }

    /**
     * called when the event of capture of picture is finished.
     * create thumbnail in AddPhotoFragment and turn into AddPhotoFragment
     *
     * @param requestCode request code
     * @param resultCode  result code
     * @param data        don't use
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 0 && resultCode == Activity.RESULT_OK) {

            addPhotoFragment.createThumbnail();
            showFragment(ADD_PHOTO_FRAGMENT_NUM);
            addPhotoFragment.setImageView();
        }

    }

    /**
     * Create Option Menu
     *
     * @param menu
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.station_menu, menu);
        this.menu = menu;
        return true;
    }

    /**
     * prepare option menu
     *
     * @param menu
     * @return
     */
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        changeActionBar(STATION_FRAGMENT_NUM);
        return super.onPrepareOptionsMenu(menu);
    }

    /**
     * call when user clicked the item of menu
     *
     * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch (id) {
            // 'update data' item clicked: request API to update data
            case R.id.action_update:
                stationFragment.requestAPI();
                break;
            // 'map' item clicked: show map
            case R.id.action_map:
                if (arr_stations == null) {
                    //TODO: shwo dialog: need update
                } else {
                    if (mapFragment == null) {
                        mapFragment = new GoogleMapFragment();
                    }
                    mapFragment.setStations(arr_stations);
                    showFragment(MAP_FRAGMENT_NUM);
                }
                break;
            // 'add photo' item clicked: open camera, take a photo
            case R.id.action_add_photo:
                openCamera();
                break;
            // 'share' item clicked: share the selected photo
            case R.id.action_share:
                shareProvider = new ShareActionProvider(this);
//                shareProvider = (ShareActionProvider)MenuItemCompat.getActionProvider(item);
                MenuItemCompat.setActionProvider(item, shareProvider);

                sharePhoto();
                break;


        }

        return super.onOptionsItemSelected(item);
    }


}
