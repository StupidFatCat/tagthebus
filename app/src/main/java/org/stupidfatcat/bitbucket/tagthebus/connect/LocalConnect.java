package org.stupidfatcat.bitbucket.tagthebus.connect;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Created by Cong on 5/21/2015.
 */

/**
 * communication with SQLite:
 * - save path of photo in sdcard
 * - get path of photo
 * - delete path of photo
 */
public class LocalConnect {
    private static final String TABLE_PHOTOS = "Photos";
    private static final String KEY_ID = "_id";
    private static final String KEY_ID_STATION = "id_station";
    private static final String KEY_NAME = "name";
    private static final String KEY_DIR = "dir";
    private static final String KEY_DIR_THUMBNAIL = "dir_thumbnail";
    private static final String KEY_DATE = "date";
    private static final String[] COLS_PHOTOS = {KEY_ID, KEY_ID_STATION, KEY_NAME, KEY_DIR, KEY_DIR_THUMBNAIL, KEY_DATE};
    private SQLiteDatabase database;
    private LocalSqlOpenHelper dbHelper;

    public LocalConnect(Context context) {
        dbHelper = new LocalSqlOpenHelper(context);
    }

    // open database and close database
    public void openWrite() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void openRead() throws SQLException {
        database = dbHelper.getReadableDatabase();
    }

    public void close() throws SQLException {
        dbHelper.close();
    }

    /**
     * add path of photo to SQLite
     *
     * @param id_station id of station
     * @param name name of photo
     * @param fullSize_path path of photo(fullsize)
     * @param thumb_path path of thumbnail
     * @param date create time
     * @return success/fail
     */
    public boolean addPhoto(int id_station, String name, String fullSize_path, String thumb_path, String date) {
        try {
            openWrite();
            ContentValues values = new ContentValues();

            values.put(KEY_ID_STATION, id_station);
            values.put(KEY_NAME, name);
            values.put(KEY_DIR, fullSize_path);
            values.put(KEY_DIR_THUMBNAIL, thumb_path);
            values.put(KEY_DATE, date);

            database.insert(TABLE_PHOTOS, null, values);
            close();

        } catch (SQLException ex) {
            Log.e("Failed in adding Type :", ex.getMessage());
            return false;
        } finally {
            close();
        }

        return true;
    }

    /**
     * get all the photos at the station (id_station)
     *
     * @param id_station id of station
     * @return data
     */
    public List<Map<String, Object>> getPhotos(int id_station) {
        List<Map<String, Object>> data;
        String path = null;
        try {
            openRead();

            Cursor cursor = database.query(TABLE_PHOTOS, COLS_PHOTOS, "id_station = " + id_station, null, null, null, null);
            data = new ArrayList<Map<String, Object>>(cursor.getCount());
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                Map<String, Object> map = new HashMap<String, Object>();
                map.put(KEY_ID, cursor.getInt(0));
                map.put(KEY_ID_STATION, cursor.getInt(1));
                map.put(KEY_NAME, cursor.getString(2));
                map.put(KEY_DIR, cursor.getString(3));
                map.put(KEY_DIR_THUMBNAIL, cursor.getString(4));
                path = cursor.getString(4);

                //create bitmap for thumbnail
                File thumbnail = new File(path);
                FileInputStream fis = new FileInputStream(thumbnail.getAbsolutePath());
                Bitmap bitmap = BitmapFactory.decodeStream(fis);
                map.put("bitmap", bitmap);
                map.put(KEY_DATE, cursor.getString(5));
                data.add(map);
                cursor.moveToNext();
            }
            return data;
        } catch (SQLException ex) {
            Log.e("Error in SQLite: ", ex.getMessage());
            return null;
        } catch (FileNotFoundException ex) {
            Log.e("Can't find the thumbnail:" + path, ex.getMessage());
            return null;
        }
    }

    /**
     * delete the photo of 'id' == id
     * @param id id of photo
     * @return success/fail
     */
    public boolean deletePhoto(int id) {
        try {
            openWrite();
            database.delete(TABLE_PHOTOS, "_id = " + id, null);
            close();
            return true;
        } catch (SQLException ex) {
            Log.e("Failed in adding Type :", ex.getMessage());
            return false;
        } finally {
            close();
        }
    }
}
