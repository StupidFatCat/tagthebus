package org.stupidfatcat.bitbucket.tagthebus.connect;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by Cong on 5/20/2015.
 */

/**
 * use to create database and table
 */
public class LocalSqlOpenHelper extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "TagTheBus";
    public static final int DATABASE_VERSON = 1;


    public LocalSqlOpenHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSON);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        try {
            String sql = "CREATE TABLE IF NOT EXISTS `Photos` (\n" +
                    "  `_id` INTEGER PRIMARY KEY AUTOINCREMENT,\n" +
                    "  `id_station` int(11) NOT NULL,\n" +
                    "  `name` varchar(100) NOT NULL,\n" +
                    "  `dir` varchar(200) NOT NULL,\n" +
                    "  `dir_thumbnail` varchar(200) NOT NULL,\n" +
                    "  `date` varchar(100) NOT NULL \n" +
                    ");";
            db.execSQL(sql);

        } catch (SQLException ex) {
            Log.e("Failed creating TABLES", ex.getMessage());
        }

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion < newVersion) {
            db.execSQL("DROP TABLE IF EXISTS " + "'Photos';");
            onCreate(db);
        }
    }
}
