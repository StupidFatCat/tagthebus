package org.stupidfatcat.bitbucket.tagthebus.connect;

import android.app.ProgressDialog;
import android.util.Log;

import com.loopj.android.http.JsonHttpResponseHandler;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.stupidfatcat.bitbucket.tagthebus.R;
import org.stupidfatcat.bitbucket.tagthebus.fragments.StationFragment;

/**
 * Created by Cong on 5/20/2015.
 */

/**
 * this class uses the library 'Android Asynchronous Http Client' to create a handler of request
 */
public class MyJsonHandler extends JsonHttpResponseHandler {
    private StationFragment myFragment;
    private ProgressDialog progressDialog;

    public MyJsonHandler(StationFragment myFragment) {
        this.myFragment = myFragment;
    }

    /**
     * show progressDialog when handler starts
     */
    @Override
    public void onStart() {
        super.onStart();
        progressDialog = new ProgressDialog(myFragment.getActivity());
        progressDialog.setMessage(myFragment.getResources().getString(R.string.dialog_please_wait));
        progressDialog.setCancelable(true);
        progressDialog.show();
    }

    /**
     * close progressDialog when handler closes
     */
    @Override
    public void onFinish() {
        super.onFinish();
        progressDialog.dismiss();
    }

    /**
     * if receive the response, check it and pass it to StationFragment to update data
     *
     * @param statusCode
     * @param headers
     * @param response
     */
    @Override
    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
        try {
            if (response.getInt("code") == 200) {
                JSONObject obj_data = response.getJSONObject("data");
                JSONArray arr_stations = obj_data.getJSONArray("nearstations");

                Log.d("get a JSONArray:", obj_data.toString());
                // pass data to StationFragment
                myFragment.updateStationList(arr_stations);
            }


        } catch (JSONException ex) {
            Log.e("get a JSON error: ", ex.getMessage());
        }
    }

    @Override
    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
        super.onFailure(statusCode, headers, throwable, errorResponse);
    }
}
