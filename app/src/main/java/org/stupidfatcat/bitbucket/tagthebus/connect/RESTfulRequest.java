package org.stupidfatcat.bitbucket.tagthebus.connect;

import com.loopj.android.http.AsyncHttpClient;

import org.stupidfatcat.bitbucket.tagthebus.fragments.StationFragment;

/**
 * Created by Cong on 5/20/2015.
 */

/**
 * this class uses the library 'Android Asynchronous Http Client' to request web server.
 */
public class RESTfulRequest {
    private static final String url_api = "http://barcelonaapi.marcpous.com/bus/nearstation/latlon/41.3985182/2.1917991/1.json";
    private static AsyncHttpClient client = new AsyncHttpClient();
    private int numSuccess = 0;

    public void getStations(StationFragment myFragment) {
        client.get(url_api, null, new MyJsonHandler(myFragment));

    }

}
