package org.stupidfatcat.bitbucket.tagthebus.fragments;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.stupidfatcat.bitbucket.tagthebus.R;
import org.stupidfatcat.bitbucket.tagthebus.connect.LocalConnect;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

/**
 * Created by Cong on 5/21/2015.
 */

/**
 * use to add photo into SQLite
 * user need to type the name of photo and click the 'Confirm' button to save.
 */
public class AddPhotoFragment extends Fragment implements View.OnClickListener {
    private static final int MAXLENGTH = 100;
    private LocalConnect localConnect;
    private View rootView;
    private File fullSizePhoto;
    private File thumbnail;
    private int id_station;
    private String title;
    private Bitmap imageBitmap;
    private Bitmap fullSizeBitmap;
    private String photoName;
    private String date;
    private OnPhotoSaveCallback mSaveCallback;
    private ImageView imageView = null;
    private Button buttonOK;

    public void setImageView() {
        if (imageView != null) imageView.setImageBitmap(fullSizeBitmap);
    }

    private void setFullSizeBitmap() {
        fullSizeBitmap = BitmapFactory.decodeFile(fullSizePhoto.getAbsolutePath());
    }

    /**
     * use fullSize photo to create a thumbnail in sdcard.
     */
    public void createThumbnail() {
        final int THUMBNAIL_SIZE = 64;
        byte[] imageData = null;

        try {

            // create thumbnail
            FileInputStream fis = new FileInputStream(fullSizePhoto.getAbsolutePath());
            imageBitmap = BitmapFactory.decodeStream(fis);

            imageBitmap = Bitmap.createScaledBitmap(imageBitmap, THUMBNAIL_SIZE, THUMBNAIL_SIZE, false);

            thumbnail = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES + "/TagTheBus/.thumbs"), photoName + ".jpg");
            if (thumbnail.exists()) {
                thumbnail.delete();
            }

            // save to sdcard
            FileOutputStream outputStream = new FileOutputStream(thumbnail);
            imageBitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);
            outputStream.flush();
            outputStream.close();


            setFullSizeBitmap();

        } catch (Exception ex) {

        }
    }

    /**
     * verify the correction of title of photo
     *
     * @return success/fail
     */
    private boolean checkTitle() {
        TextView textView = (TextView) rootView.findViewById(R.id.editTitle);
        title = textView.getText().toString();
        if (title.length() == 0) {
            Toast toast = Toast.makeText(getActivity(), "title is empty.", Toast.LENGTH_SHORT);
            toast.show();
            return false;
        } else if (title.length() > MAXLENGTH) {
            Toast toast = Toast.makeText(getActivity(), "title length must short than 100.", Toast.LENGTH_SHORT);
            toast.show();
            return false;
        }
        return true;
    }

    /**
     * save the path of photo to SQLite
     */
    private void saveToSQLite() {
        if (localConnect.addPhoto(id_station, title, fullSizePhoto.getAbsolutePath(), thumbnail.getAbsolutePath(), date)) {
            Toast toast = Toast.makeText(getActivity(), "saved.", Toast.LENGTH_SHORT);
            toast.show();
        } else {
            Toast toast = Toast.makeText(getActivity(), "can't save.", Toast.LENGTH_SHORT);
            toast.show();
            if (fullSizePhoto.exists()) {
                fullSizePhoto.delete();
            }
            if (thumbnail.exists()) {
                thumbnail.delete();
            }
        }
    }

    /**
     * check whether the 'Confirm' button is clicked.
     *
     * @param v
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.buttonOK:
                if (checkTitle()) {
                    saveToSQLite();
                    mSaveCallback.onPhotoSaveCallback();
                }
                break;
        }
    }


    public void setIdStation(int id_station) {
        this.id_station = id_station;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setPhotoName(String photoName) {
        this.photoName = photoName;
    }

    public void setFullSizePhoto(File fullSizePhoto) {
        this.fullSizePhoto = fullSizePhoto;
    }

    public void setLocalConnect(LocalConnect localConnect) {
        this.localConnect = localConnect;
    }

    /**
     * attach to MainActivity to callback at sometime
     *
     * @param activity
     */
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mSaveCallback = (OnPhotoSaveCallback) activity;
        } catch (ClassCastException ex) {
            throw new ClassCastException(activity.toString() + " must implement callback");
        }
    }

    /**
     * create view of fragment
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.add_photo_fragment, container, false);
        imageView = (ImageView) rootView.findViewById(R.id.image_to_add);
        buttonOK = (Button) rootView.findViewById(R.id.buttonOK);

        buttonOK.setOnClickListener(this);
        setImageView();
        return rootView;
    }

    /**
     * the interface to callback
     */
    public interface OnPhotoSaveCallback {
        void onPhotoSaveCallback();
    }
}
