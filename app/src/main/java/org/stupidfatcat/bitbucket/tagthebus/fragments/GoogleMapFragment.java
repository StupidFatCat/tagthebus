package org.stupidfatcat.bitbucket.tagthebus.fragments;


import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.stupidfatcat.bitbucket.tagthebus.R;

/**
 * Created by Cong on 5/21/2015.
 */

/**
 * use to show googlemap, add markers on the map
 */
public class GoogleMapFragment extends Fragment implements OnMapReadyCallback {
    private static final LatLng CITY_LOCATION = new LatLng(41.3985182, 2.1917991);
    private View rootView;
    private GoogleMap googleMap = null;
    private SupportMapFragment mMapFragment;
    private JSONArray arr_stations;
    private StationFragment.OnStationClickedCallback mClickedCallback;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.map_fragment, container, false);

        mMapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        if (mMapFragment != null) {
            mMapFragment.getMapAsync(this);
        } else {
            Log.i("main", "supportMapFragment is null.");
        }

        return rootView;
    }

    /**
     * attach to MainActivity to callback sometime
     * @param activity
     */
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mClickedCallback = (StationFragment.OnStationClickedCallback) activity;
        } catch (ClassCastException ex) {
            throw new ClassCastException(activity.toString() + " must implement callback");
        }
    }

    /**
     * if map is ready, set info window and add markers
     *
     * @param googleMap
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        try {
            googleMap.setMyLocationEnabled(true);

            // set info window
            googleMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
                @Override
                public View getInfoWindow(Marker marker) {
                    return null;
                }

                @Override
                public View getInfoContents(Marker marker) {
                    LinearLayout view = (LinearLayout) getLayoutInflater(null).inflate(R.layout.info_window, null);
                    TextView name = (TextView) view.findViewById(R.id.titleMap);
                    name.setText(marker.getTitle());
                    return view;
                }


            });

            // set click listener of InfoWindow in order to turn into PhotoListFragment
            googleMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                        public void onInfoWindowClick(Marker marker) {
                            int id = Integer.parseInt(marker.getSnippet());
                            mClickedCallback.onStationClickedCallback(id, marker.getTitle());
                        }
                    });

            // prepare Markers
            for (int i = 0; i < arr_stations.length(); i++) {
                JSONObject obj_station = arr_stations.getJSONObject(i);
                LatLng position = new LatLng(obj_station.getDouble("lat"), obj_station.getDouble("lon"));
                String street_name = obj_station.getString("street_name");
                int id = obj_station.getInt("id");
                String snippet = Integer.toString(id);

                //add Markers
                googleMap.addMarker(new MarkerOptions().position(position).title(street_name).snippet(snippet));
                CameraUpdate center = CameraUpdateFactory.newLatLng(CITY_LOCATION);
                CameraUpdate zoom = CameraUpdateFactory.zoomTo(15);
                googleMap.moveCamera(center);
                googleMap.animateCamera(zoom);
            }

        } catch (JSONException ex) {
            Log.e("Caught a JSONException:", ex.getMessage());
        }

    }

    public void setStations(JSONArray arr_stations) {
        this.arr_stations = arr_stations;
    }
}
