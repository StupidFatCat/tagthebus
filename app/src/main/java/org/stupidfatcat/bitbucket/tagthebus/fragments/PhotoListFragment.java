package org.stupidfatcat.bitbucket.tagthebus.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import org.stupidfatcat.bitbucket.tagthebus.R;
import org.stupidfatcat.bitbucket.tagthebus.connect.LocalConnect;

import java.util.List;
import java.util.Map;

/**
 * Created by Cong on 5/21/2015.
 */

/**
 * use to list all the photos of specific station (id_station)
 */
public class PhotoListFragment extends Fragment implements AdapterView.OnItemClickListener, AdapterView.OnItemLongClickListener {
    private ListView photo_listView = null;

    private int id_station;
    private String street_name;

    private LocalConnect localConnect;

    private OnPhotoClickedCallback mClickedCallback;

    /**
     * load all the photo
     */
    public void loadPhotoList() {
        // get data from SQLite
        List<Map<String, Object>> data = localConnect.getPhotos(id_station);

        //prepare Adapter
        if (photo_listView != null && data != null && data.size() != 0) {
            SimpleAdapter photoList_Adapter = new SimpleAdapter(getActivity(), data, R.layout.list_photo, new String[]{"name", "bitmap", "date"}, new int[]{R.id.title, R.id.pic, R.id.date});

            photo_listView.setAdapter(photoList_Adapter);
            photoList_Adapter.setViewBinder(new SimpleAdapter.ViewBinder() {
                // add photo to ImageView
                @Override
                public boolean setViewValue(View view, Object data, String textRepresentation) {
                    if (view instanceof ImageView && data instanceof Bitmap) {
                        ImageView imageView = (ImageView) view;
                        imageView.setImageBitmap((Bitmap) data);
                        return true;
                    } else {
                        return false;
                    }

                }
            });

            // set listener: click to show fullSize photo; long click to show delete dialog
            photo_listView.setOnItemClickListener(this);
            photo_listView.setOnItemLongClickListener(this);
        } else if (photo_listView != null && (data == null || data.size() == 0)) {
            photo_listView.setAdapter(null);
        }


    }

    /**
     * click to callback:
     * pass path and name to PhotoViewFragment
     * and turn into PhotoViewFragment
     *
     * @param parent
     * @param view
     * @param position
     * @param id
     */
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Map<String, Object> map = (Map<String, Object>) photo_listView.getItemAtPosition(position);
        Toast.makeText(getActivity(), map.get("name") + " clicked.", Toast.LENGTH_SHORT).show();
        mClickedCallback.onPhotoClickedCallback((String) map.get("name"), (String) map.get("dir"));

    }

    /**
     * long click to show delete dialog
     * confirm to delete
     *
     * @param parent
     * @param view
     * @param position
     * @param id
     * @return
     */
    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
        Map<String, Object> map = (Map<String, Object>) photo_listView.getItemAtPosition(position);
        Toast.makeText(getActivity(), map.get("name") + " long clicked.", Toast.LENGTH_SHORT).show();
        String name = (String) map.get("name");
        final int id_photo = (int) map.get("_id");

        // prepare dialog
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("delete: " + name).setMessage("Are you sure?").setIcon(android.R.drawable.ic_dialog_alert).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if (localConnect.deletePhoto(id_photo)) {
                    Toast.makeText(getActivity(), "deleted.", Toast.LENGTH_SHORT).show();
                    loadPhotoList();
                } else {
                    Toast.makeText(getActivity(), "delete failed.", Toast.LENGTH_SHORT).show();
                }
            }
        }).setNegativeButton("No", null)                        //Do nothing on no
                .show();
        return false;
    }

    public int getIdStation() {
        return id_station;
    }

    public void setIdStation(int id_station) {
        this.id_station = id_station;
    }

    public String getStreetName() {
        return street_name;
    }

    public void setStreetName(String street_name) {
        this.street_name = street_name;
    }

    public void setLocalConnect(LocalConnect localConnect) {
        this.localConnect = localConnect;
    }

    /**
     * attach to MainActivity to callback sometime
     *
     * @param activity
     */
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mClickedCallback = (OnPhotoClickedCallback) activity;
        } catch (ClassCastException ex) {
            throw new ClassCastException(activity.toString() + " must implement callback");
        }
    }

    /**
     * create view of fragment
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.photo_fragment, container, false);

        photo_listView = (ListView) rootView.findViewById(R.id.photoListView);
        loadPhotoList();
        return rootView;
    }

    /**
     * interface to callback
     */
    public interface OnPhotoClickedCallback {
        void onPhotoClickedCallback(String name, String dir);
    }
}
