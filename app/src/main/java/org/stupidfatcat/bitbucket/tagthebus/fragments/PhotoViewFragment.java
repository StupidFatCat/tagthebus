package org.stupidfatcat.bitbucket.tagthebus.fragments;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import org.stupidfatcat.bitbucket.tagthebus.R;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

/**
 * Created by Cong on 5/21/2015.
 */

/**
 * use to show the fullSize photo from photoList
 */
public class PhotoViewFragment extends Fragment {
    private View rootView;

    private String name;    // name of photo
    private String dir;     // path of photo

    private ImageView imageView = null;

    // create bitmap and load into ImageView
    public void loadImage() {

        if (imageView != null) {
            try {
                //create bitmap for fullSize Photo
                File photo = new File(dir);
                FileInputStream fis = new FileInputStream(photo.getAbsolutePath());
                Bitmap bitmap = BitmapFactory.decodeStream(fis);

                //load it
                imageView.setImageBitmap(bitmap);
            } catch (FileNotFoundException ex) {
                Log.e("Can't find the photo:" + dir, ex.getMessage());
            }
        }
    }

    public void setDir(String dir) {
        this.dir = dir;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /**
     * create vidw of fragment
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.photo_view, container, false);
        imageView = (ImageView) rootView.findViewById(R.id.photoView);
        loadImage();
        return rootView;
    }
}
