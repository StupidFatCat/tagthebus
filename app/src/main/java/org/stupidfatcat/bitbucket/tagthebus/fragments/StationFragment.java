package org.stupidfatcat.bitbucket.tagthebus.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.stupidfatcat.bitbucket.tagthebus.R;
import org.stupidfatcat.bitbucket.tagthebus.connect.RESTfulRequest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Cong on 5/21/2015.
 */

/**
 * use to list all the stations from request
 */
public class StationFragment extends Fragment implements AdapterView.OnItemClickListener {
    private View rootView;
    private RESTfulRequest restfulRequest;
    private ListView station_listView;
    private SimpleAdapter station_Adapter;

    private OnStationsReadyCallback mReadyCallback;
    private OnStationClickedCallback mClickedCallback;

    private boolean isUploaded = false;

    /**
     * request RESTful API web server
     */
    public void requestAPI() {
        restfulRequest.getStations(this);
    }

    /**
     * update the station list in fragment
     *
     * @param arr_stations
     */
    public void updateStationList(JSONArray arr_stations) {
        station_Adapter = new SimpleAdapter(getActivity(), getData(arr_stations), android.R.layout.simple_list_item_1, new String[]{"street_name"}, new int[]{android.R.id.text1});

        station_listView.setAdapter(station_Adapter);
        station_listView.setOnItemClickListener(this);
        mReadyCallback.onStationsReadyCallback(arr_stations);
        isUploaded = true;
    }

    /**
     * transform the data(JSONArray) to List<>
     *
     * @param arr_stations
     * @return
     */
    private List<Map<String, Object>> getData(JSONArray arr_stations) {
        List<Map<String, Object>> list = new ArrayList<Map<String, Object>>(arr_stations.length());

        try {
            for (int i = 0; i < arr_stations.length(); i++) {
                JSONObject obj_station = arr_stations.getJSONObject(i);
                Map<String, Object> map = new HashMap<String, Object>();
                map.put("id", obj_station.getInt("id"));
                map.put("street_name", obj_station.getString("street_name"));

                list.add(map);
            }

            return list;

        } catch (JSONException ex) {
            Log.e("Caught a JSONException:", ex.getMessage());
            return list;
        }
    }

    /**
     * click to callback:
     * pass the id_station and street_name to PhotoListFragment
     * and turn into PhotoListFragment
     *
     * @param parent
     * @param view
     * @param position
     * @param id
     */
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Map<String, Object> map = (Map<String, Object>) station_listView.getItemAtPosition(position);
        Toast.makeText(getActivity(), map.get("street_name") + " clicked.", Toast.LENGTH_SHORT).show();
        mClickedCallback.onStationClickedCallback((int) map.get("id"), (String) map.get("street_name"));
    }

    /**
     * attach to MainActivity in order to callback sometime
     *
     * @param activity
     */
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mReadyCallback = (OnStationsReadyCallback) activity;
            mClickedCallback = (OnStationClickedCallback) activity;
        } catch (ClassCastException ex) {
            throw new ClassCastException(activity.toString() + " must implement callback");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    /**
     * create view of fragment
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.station_fragment, container, false);

        restfulRequest = new RESTfulRequest();
        station_listView = (ListView) rootView.findViewById(R.id.stationListView);
        if (!isUploaded) {
            requestAPI();
        }
        return rootView;

    }

    /**
     * interface to callback
     */
    public interface OnStationsReadyCallback {
        void onStationsReadyCallback(JSONArray arr_stations);
    }

    /**
     * interface to callback
     */
    public interface OnStationClickedCallback {
        void onStationClickedCallback(int id_station, String street_name);
    }
}
